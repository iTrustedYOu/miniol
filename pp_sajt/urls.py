from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    #url(r'^', include('pp_sajt.apps.main.urls',namespace="main")),
    url(r'^blog', include('pp_sajt.apps.blog.urls', namespace="blog")),
    url(r'^main', include('pp_sajt.apps.main.urls', namespace="main")),
                
 
 )
