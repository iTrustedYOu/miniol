from django.db import models
from django.shortcuts import resolve_url
# Create your models here.


class Blog(models.Model):

    def get_absolute_url(self):
        return resolve_url('blog:blog-view-blog',self.id)
        
    

class BlogEntry(models.Model):
    text = models.TextField()
    pub_date = models.DateTimeField()
    blog = models.ForeignKey(Blog)
    
    def __unicode__(self):
        return self.text

    def save(self,*args,**kwargs):
        self.full_clean()
        super(BlogEntry,self).save(*args,**kwargs)