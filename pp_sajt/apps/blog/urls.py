from django.conf.urls import patterns, url, include

from django.contrib import admin
admin.autodiscover()

from . import views

urlpatterns = patterns('',
                       #url(r'^$',views.index,name='blog-index'),
                       # url(r'^login$',views.redirect_login,name="redirect_login"),
                       # url(r'^user_login$',views.user_login,name="user_login"),
                       # url(r'^(?P<url>\w+)/$',views.activity,name='activity'),
                       # url(r'^user_logout$',views.user_logout,name="user_logout"),
                       url(r'^/$',views.index,name="blog-index"),
                       url(r'^/add_new_blog_entry/',views.add_blog_entry,name="blog-new-entry"),
                       url(r'^/(\d+)/$',views.view_blog,name="blog-view-blog"),

                       
                       )