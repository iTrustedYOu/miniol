from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from models import BlogEntry, Blog
from django.utils.timezone import now
from django.core.exceptions import ValidationError
# Create your views here.
def index(request):
    return render(request, "blog/index.djhtml")

def add_blog_entry(request):
    if request.method=='POST':
        blog = Blog.objects.create()
        try:
            BlogEntry.objects.create(text = request.POST['blog-entry'],pub_date = now(),blog=blog)
        except ValidationError:
            error_text = "You can't post an empty blog entry"
            return render(request,'blog/index.djhtml', {'error':error_text})
        return redirect(blog)
  
    return redirect(reverse('blog:blog-index'))

def view_blog(request,blog_id):
    blog = Blog.objects.get(id = blog_id)
    error_text = None
    if request.method=='POST':
        try:
            BlogEntry.objects.create(text = request.POST['blog-entry'],pub_date = now(),blog=blog)
            return redirect(blog)
        except:
            error_text = "You can't post an empty blog entry"
    return render(request,"blog/blogs.djhtml",{'blog':blog,'error':error_text})
   
    
