from django import forms

from pp_sajt.apps.blog.models import BlogEntry


EMPTY_BLOG_POST_ERROR = "You can't post an empty blog entry!"



class BlogEntryForm(forms.models.ModelForm):

    class Meta:
        model = BlogEntry
        fields = ('text',)
        widgets = {
            'text': forms.Textarea(attrs={'class':"form-control input-lg",
                                          'placeholder':"Enter a new blog entry"}),
        }


    def __init__(self,*args,**kwargs):
        super(BlogEntryForm,self).__init__(*args,**kwargs)
        empty_error = EMPTY_BLOG_POST_ERROR
        self.fields['text'].error_messages['required'] = empty_error
