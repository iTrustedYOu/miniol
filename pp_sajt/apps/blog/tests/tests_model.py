from django.test import TestCase
from django.utils.timezone import now
from pp_sajt.apps.blog.models import BlogEntry,Blog
from django.core.exceptions import ValidationError

class BlogAndBlogEntryTest(TestCase):


    def test_get_absolute_url(self):
        blog1 = Blog.objects.create()
        self.assertEqual(blog1.get_absolute_url(),'/blog/%d/' % blog1.id)

    def test_cannot_post_empty_blog_entry(self):
        blog1 = Blog.objects.create()
        entry = BlogEntry(blog = blog1, text="",pub_date=now())
        with self.assertRaises(ValidationError):
            entry.save()

    def test_saving_and_retrieving(self):
        blog = Blog()
        blog.save()
        
        first_blog_post = BlogEntry()
        first_blog_post.text = "The first (ever) blog post"
        first_blog_post.pub_date = now()
        first_blog_post.blog = blog
        first_blog_post.save()

        second_blog_post = BlogEntry()
        second_blog_post.text = "The second (ever) blog post"
        second_blog_post.pub_date = now()
        second_blog_post.blog = blog
        second_blog_post.save()

        saved_blogs = Blog.objects.all()
        self.assertEqual(saved_blogs.count(), 1)
        self.assertEqual(saved_blogs[0], blog)

        saved_items = BlogEntry.objects.all()
        self.assertEqual(saved_items.count(),2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]

        self.assertEqual(first_saved_item.text, "The first (ever) blog post")
        self.assertEqual(first_saved_item.blog,blog)
        self.assertEqual(second_saved_item.text,"The second (ever) blog post")
        self.assertEqual(second_saved_item.blog,blog)

    