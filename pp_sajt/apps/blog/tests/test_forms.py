from django.test import TestCase

from pp_sajt.apps.blog.forms import BlogEntryForm, EMPTY_BLOG_POST_ERROR


class BlogEntryFormTest(TestCase):

    def test_form_renders_item_text_input(self):
        form = BlogEntryForm()
        self.assertIn('placeholder="Enter a new blog entry"',form.as_p())
        self.assertIn('class="form-control input-lg"',form.as_p())

    def test_form_validation_for_blank_items(self):
        form = BlogEntryForm(data={'text':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['text'],[EMPTY_BLOG_POST_ERROR]
            )
        self.fail("http://chimera.labs.oreilly.com/books/1234000000754/ch10.html#_using_the_form_in_a_view_with_a_get_request")
        