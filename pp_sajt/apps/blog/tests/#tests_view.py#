from django.test import TestCase
from django.http import HttpRequest
from django.template.loader import render_to_string
from pp_sajt.apps.blog.views import index,add_blog_entry,view_blog
from django.core.urlresolvers import reverse
from django.utils.timezone import now
from django.utils.html import escape

from pp_sajt.apps.blog.models import BlogEntry,Blog




class IndexPageTest(TestCase):

    def test_blog_entry_added_only_when_necessary(self):
        request = HttpRequest()
        add_blog_entry(request)
        self.assertEqual(BlogEntry.objects.all().count(),0)

    
    def test_index_page_returns_correct_url(self):
        request = HttpRequest()
        request.method = 'GET'
        response = index(request)
        expected_html = render_to_string('blog/index.djhtml')
        self.assertEqual(response.content.decode(),expected_html)



    

class BlogViewTest(TestCase):

    
    def test_uses_blog_template(self):
        blog = Blog.objects.create()
        response = self.client.get('/blog/%d/' % (blog.id,))
        #self.assertTemplateUsed(response,'blog/blogs.djhtml') 

    def test_displays_only_correct_blog_posts(self):
        correct_blog = Blog.objects.create()
        BlogEntry.objects.create(text="New blog entry",blog = correct_blog,pub_date = now())
        BlogEntry.objects.create(text="New blog entry 2",blog = correct_blog, pub_date = now())

        other_blog = Blog.objects.create()
        BlogEntry.objects.create(text = "Whatever blog entry", blog= other_blog,pub_date=now())
        BlogEntry.objects.create(text = "Whatever two blog entry", blog= other_blog,pub_date=now())

        response = self.client.get('/blog/%d/' % (correct_blog.id,))

        self.assertIn( 'New blog entry',response.content.decode())
        self.assertIn( 'New blog entry 2',response.content.decode())
        self.assertNotIn( 'Whatever blog entry',response.content.decode())
        self.assertNotIn( 'Whatever two blog entry',response.content.decode())


    def test_passes_correct_blog_to_template(self):
        other_blog = Blog.objects.create()
        correct_blog = Blog.objects.create()

        response = self.client.get('/blog/%d/' % correct_blog.id)
        self.assertEqual(response.context['blog'],correct_blog)



    def test_can_save_a_post_request_to_existing_blog(self):
        other_blog = Blog.objects.create()
        correct_blog = Blog.objects.create()

        self.client.post(
            '/blog/%d/' % correct_blog.id,
            data = {'blog-entry':"A new blog entry for existing blog"}
        )
        self.assertEqual(BlogEntry.objects.all().count(),1)
        new_entry = BlogEntry.objects.all()[0]
        self.assertEqual(new_entry.text,"A new blog entry for existing blog")
        self.assertEqual(new_entry.blog,correct_blog)

    def test_POST_redirects_to_list_view(self):
        
        other_blog = Blog.objects.create()
        correct_blog = Blog.objects.create()
        response = self.client.post(
            '/blog/%d/' % correct_blog.id,
            data = {'blog-entry':"A new blog entry for existing blog"}
        )

        self.assertRedirects(response, '/blog/%d/' % correct_blog.id)


    def test_redirects_after_POST(self):
        response = self.client.post(
            '/blog/add_new_blog_entry/',
            data={'blog-entry': 'A new blog entry'}
        )
        self.assertEqual(Blog.objects.all().count(), 1)
        new_blog = Blog.objects.all()[0]
        self.assertRedirects(response,'/blog/%d/' % new_blog.id)

    def test_saving_a_POST_request(self):
        self.client.post(
            '/blog/add_new_blog_entry/',
            data={'blog-entry': 'A new blog entry'}
        )
        self.assertEqual(Blog.objects.all().count(), 1)

        self.assertEqual(BlogEntry.objects.all().count(), 1)
        new_item = BlogEntry.objects.all()[0]
        self.assertEqual(new_item.text, 'A new blog entry')



    def test_validation_errors_sent_back_to_home_page_template(self):
        blog = Blog.objects.create()
        
        response = self.client.post('/blog/%d/'% blog.id,data={'blog-entry':''})
        self.assertEqual(BlogEntry.objects.all().count(),0)
        self.assertTemplateUsed(response,'blog/blogs.djhtml')
        expected_error = escape("You can't post an empty blog entry")
        self.assertContains(response,expected_error)




