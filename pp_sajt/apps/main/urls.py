
from django.conf.urls import patterns, url, include

from pp_sajt.apps.main import views

# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$',views.index,name='index'),
                       # url(r'^login$',views.redirect_login,name="redirect_login"),
                       url(r'^/user_login$',views.user_login,name="user_login"),
                       # url(r'^(?P<url>\w+)/$',views.activity,name='activity'),
                       url(r'^comments_add$',views.add_comment,name="comments_add"),
                       url(r'^user_logout$',views.user_logout,name="user_logout"),
                       # url(r'^admin/', include(admin.site.urls)),
                       #url(r'^blog/',include(pp_sajt.apps.blog.urls)),
                       url(r'^/inactive_user',views.inactive_user, name="inactive-user"),
                       )
