from django.test import TestCase
from django.test import LiveServerTestCase
# from selenium.webdriver.firefox.webdriver import WebDriver
from pp_sajt.apps.main.views import index,add_comment,user_logout
from pp_sajt.apps.main.models import Comment
from django.core.urlresolvers import resolve,reverse
from django.http import HttpRequest
from django.contrib.auth.models import User,UserManager
import time


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.template.loader import render_to_string
    
class HomePageTest(TestCase):
    """Unit tests, samo testiramo delove koji
    mogu sami po sebi da se testiraju, za razliku
    od funkcionalnih testova gde je potrebna neka
    komunikacija i stanje i akcije user-a"""
    
    def create_user(self,username="johndoe",password="johndoe",is_active=True):
        self.assertEqual(User.objects.all().count(),0)
        User.objects.create_user(username=username,password=password)
        
        self.assertEqual(User.objects.all().count(),1)
        new_user = User.objects.get(username="johndoe")
        new_user.is_active = is_active
        new_user.save()
        self.assertEqual(new_user.username,username)

    def test_main_url_resolvers_to_index_view(self):
        found = resolve('/main')
        self.assertEqual(found.func,index)
    
    def test_index_page_returns_correct_url(self):
        request = HttpRequest()
        request.method = 'GET'
        response = index(request)
        expected_html = render_to_string('main/index.djhtml')
        # convert response.content bytes to python unicode string
        self.assertEqual(response.content.decode(),expected_html)
    
    def test_index_page_redirects_after_a_POST(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['comment'] = "A new comment"
        response = add_comment(request)
    
        self.assertEqual(response.status_code,302)
        self.assertEqual(response['location'],'/')


    def is_logged_in(self,username):
        resp =self.client.get(reverse('main:index'))
        return User.objects.get(username="johndoe") == resp.context['user']

    def test_user_not_logged_in_initially(self):
        self.create_user()
        self.assertFalse(self.is_logged_in(username="johndoe"))

    def test_index_page_can_login_an_active_user_via_POST(self):
        self.create_user()
        response = self.client.post(
            '/main/user_login',
            data={'username':"johndoe",
                  'password':"johndoe",
                  }
        )

        resp =self.client.get(response['location'])
        self.assertTrue(self.is_logged_in("johndoe"))

    def test_index_page_redirects_after_a_user_login(self):
        self.create_user()
        response = self.client.post(
            '/main/user_login',
            data={'username':"johndoe",
                  'password':"johndoe",
                  }
        )

        self.assertRedirects(response,reverse('main:index'))


    def test_inactive_user_cant_login(self):
        self.create_user(is_active=False)
        response = self.client.post(
            '/main/user_login',
            data={'username':"johndoe",
                  'password':"johndoe",
                  }
        )

        self.assertFalse(self.is_logged_in("johndoe"))

    def test_index_page_redirects_after_inactive_user_login(self):
        self.create_user(is_active=False)
        response = self.client.post(
            '/main/user_login',
            data={'username':"johndoe",
                  'password':"johndoe",
                  }
        )

        self.assertRedirects(response,reverse('main:inactive-user'))


    def test_index_page_returns_to_index_with_error_when_bad_login(self):
        response = self.client.post(
            '/main/user_login',
            data={'username':"johndoe",
                  'password':"johndoe",
                  }
        )
        self.assertTemplateUsed(response,'main/index.djhtml')
        self.assertEqual(response.context['error'],"Bad login!")
        self.assertContains(response,"Bad login!")

        

    # def test_index_page_displays_all_comments(self):
    #     Comment.objects.create(text="itemey 1")
    #     Comment.objects.create(text="itemey 2")
    #     request = HttpRequest()
    #     response = index(request)
    #     self.assertIn('itemey 1', response.content.decode())
    #     self.assertIn('itemey 2', response.content.decode())
        

    # def test_index_page_can_save_a_POST_request(self):
    #     request = HttpRequest()
    #     #user = User()
    #     #user = UserManager().create_user("tara","tara.petric@gmail.com","tara")
    #     #request.user = user
    #     request.method='POST'
    #     request.POST['comment'] = "A new comment"
    #     add_comment(request)
    #     response = index(request)
    #     self.assertIn("A new comment",response.content.decode(),msg=response.content.decode())

class CommentModelTest(TestCase):

    def test_saving_and_retrieving(self):
        first_comment = Comment()
        first_comment.text = "The first (ever comment)"
        first_comment.save()

        second_comment = Comment()
        second_comment.text = "Comment the second"
        second_comment.save()

        saved_items = Comment.objects.all()
        self.assertEqual(saved_items.count(),2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.text, "The first (ever comment)")
        self.assertEqual(second_saved_item.text,"Comment the second")

        
    
    


        
