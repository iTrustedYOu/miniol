from .base import FunctionalTest


class LayoutAndStylingTest(FunctionalTest):
                  
    def test_layout_and_styling(self):
        # Edith goes to the blog page
        self.browser.get(self.server_url+'/blog')
        self.browser.set_window_size(1024,768)
        # She notices that the input box is nicely centered
        textarea = self.browser.find_element_by_tag_name('textarea')
        self.assertAlmostEqual(
            textarea.location['x'] + textarea.size['width']/2,
            512,
            delta = 3
            )
        
