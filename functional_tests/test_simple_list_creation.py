from .base import FunctionalTest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys



class NewVisitorTest(FunctionalTest):
        
    # def test_can_login(self):
    
    #     # Student hoce da pristupi kursu 'Programski Prevodioci' i svom
    #     # profilu, pa ode na homepage
        
    #     self.browser.get(self.live_server_url)
    #     # Primecuje rec Programski prevodioci u title-u
    #     self.assertIn("Programski prevodioci",self.browser.title)
    #     header_text = self.browser.find_element_by_tag_name('h1').text
    #     self.assertIn("Programski prevodioci",header_text)
        
        
        
    #     # 1. Nije ulogovan i zeli da se uloguje
    #     #    a) Vidi login formular sa desne strane
    #     username_input = self.browser.find_element_by_css_selector('input[name=username]')
        
    #     password_input = self.browser.find_element_by_css_selector('input[name=password]')
    #     #self.assertEqual(username_input.get_attribute('placeholder'),"Unesite vasu lozinku...")
    #     #    b) Kuca username i password
    #     username_input.send_keys("john")
    #     password_input.send_keys("john")
    #     #    c) Pritiska submit dugme ili kucka Enter
    #     self.browser.find_element_by_css_selector("input[name=login-submit]").click()
    #     #password_input.send_keys(Keys.ENTER)
    #     #    d) i. Pogresan login - stranica updejtovana sa error messages
    #     # Proveri da li su se stvorili error tagovi
    #     #       ii.Uspesan login - stranica updejtovana i sada je customizovana za studenta
    #     #          I. Student primecuje:
    #     #            1)U title se pojavljuje ime studenta
    #     #            2) Navbar sadrzi njegovu sliku i notifikacije, i mozda jos nesto
    #     #            3) Progress bar sa kompletiranim aktivnostima
    #     #            4) Customizovan comment box
    #     comment_input_box = self.browser.find_element_by_css_selector('.comment-placeholder')
    #     self.assertEqual(comment_input_box.get_attribute('placeholder'),'Write a comment...')
    #     #         II.Sledece akcije:
    #     #            1)Student hoce da vidi detalje svog profila
    #     #            2)Student hoce da ukuca komentar
    #     #              i)Student kuca komentar i prit        iska post ili Enter
    #     time.sleep(3)
    #     comment_input_box.send_keys("Dobar dan svima!")
    #     self.browser.find_element_by_css_selector("input[name=comment-submit]").click()
    #     time.sleep(3)
    #     comment = self.browser.find_element_by_css_selector("div.comment-container:first-child p:nth-of-type(2)")

    #     self.assertEqual("Dobar dan svima!",comment.text,msg="New comment did not appear at top of page")
        
        
    #     #                 a)Student nije ukucao nista - disejblovano post dugme
    #     #                 b)I kad pritisne enter mu izadje greska
    #     #              ii)Student primecuje da je njegov komentar dodat
    #     #            3)Student hoce da uradi aktivnost
    #     #              i) Student bira jednu od aktivnosti iz bilo kog modula
    #     #              ii) Browser ode na stranicu aktivnosti
    #     self.fail("Finish writting tests!")

    def test_can_start_a_blog_and_retrieve_it_later(self):
        self.browser.get(self.server_url+ '/blog')
        input_box = self.browser.find_element_by_css_selector('textarea')
        #User comes to the homepage, and wants to make a
        # blog entry. He goes to the blog url and starts
        # typing his entry
        input_box.send_keys("Ovo je moj prvi blog entry")
        # As soon as he submits, a new blog is created. This
        self.browser.find_element_by_css_selector('input[type=submit]').click()
        # blog has a specific url.
        newuser_blog_url = self.browser.current_url
        self.assertRegexpMatches(newuser_blog_url, '/blog/.+')
        self.check_for_blog_entry('Ovo je moj prvi blog entry')
        # User wants to make another blog entry. He types something in t
        # the text area
        input_box = self.browser.find_element_by_css_selector('textarea')
        input_box.send_keys("Ovo je moj drugi blog entry")
        # he submits his blog entry
        self.browser.find_element_by_css_selector('input[type=submit]').click()
        newuser_blog_url = self.browser.current_url
        self.assertRegexpMatches(newuser_blog_url, '/blog/.+')
        # sad su mu oba blog entry-ja tu
        self.check_for_blog_entry('Ovo je moj drugi blog entry')
        self.check_for_blog_entry('Ovo je moj prvi blog entry')

        # That user is finished with his session
        self.browser.quit()
        # Another user opens his browser, and begins his session
        self.browser = webdriver.Firefox()
        # Francis visits the blog page, and there is no sign
        # of previous blog entries
        self.browser.get( self.server_url + '/blog')
        self.browser.implicitly_wait(3)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Ovo je moj prvi blog entry',page_text)
        self.assertNotIn('Ovo je moj drugi blog entry',page_text)

        # Francis pocinje novi blog tako sto ubacuje novi blog post

        self.browser.find_element_by_css_selector('textarea')
        #input_box.send_keys('Buy milk')
        self.browser.find_element_by_css_selector('input[type=submit]').click()

        # Francis dobija svoj jedinstevni url
        francis_list_url = self.browser.current_url
        self.assertRegexpMatches(francis_list_url,'/blog/.+')
        self.assertNotEqual(francis_list_url,newuser_blog_url)
        # Again , there is no trace of previous user list
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Ovo je moj prvi blog entry', page_text)
        #self.assertIn('Buy milk', page_text)

        # Zadovoljan, prvi korisnik se vrati na spavanje. Njegova lista je
        # na sigurnom


