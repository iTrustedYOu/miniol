from .base import FunctionalTest

class ItemValidationTest(FunctionalTest):
        
    def test_cannot_add_empty_blog_entry(self):
        # Edith comes back to the blog home page and accidentaly
        # tries to submit an empty blog entry. She hits submit,
        # on an empty textarea...

        self.browser.get(self.server_url + '/blog/')
        self.browser.find_element_by_css_selector('input[type=submit]').click()
        
        # The blog home page refreshes and there is an error
        # saying that the blog entry cannot be blank

        error = self.browser.find_element_by_css_selector('.has-error')
        self.assertEqual(error.text,"You can't post an empty blog entry")
        
        # She tries again after inputing some text
        # And it works
        self.browser.find_element_by_tag_name('textarea').send_keys('Buy milk')
        self.browser.find_element_by_css_selector('input[type=submit]').click()
        self.check_for_blog_entry('Buy milk') 
        

        # Perversly she now tries to input a second blank entry

        self.browser.find_element_by_css_selector('input[type=submit]').click()

        # She receives a similiar warning
        self.check_for_blog_entry('Buy milk')
        error = self.browser.find_element_by_css_selector('.has-error')
        self.assertEqual(error.text,"You can't post an empty blog entry")
        
        # And she can correct it by filling some text in
        self.browser.find_element_by_tag_name('textarea').send_keys('Make tea')
        self.browser.find_element_by_css_selector('input[type=submit]').click()
        self.check_for_blog_entry('Buy milk')
        self.check_for_blog_entry('Make tea')


